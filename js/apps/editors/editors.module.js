define([
	// Defaults
	"jquery",
	"angular",
	"mediumEditor",
	"mediumInsert",
	"ngTags"
], function($, ng, MediumEditor){
	"use strict";

	function extend(b, a) {
	    var prop;
	    if (b === undefined) {
	        return a;
	    }
	    for (prop in a) {
	        if (a.hasOwnProperty(prop) && b.hasOwnProperty(prop) === false) {
	            b[prop] = a[prop];
	        }
	    }
	    return b;
	}

	function titleEditorApp(element, options) {
		this.init(element, options);
	}

	titleEditorApp.prototype = {
		$el : $('.title-holder'),
		defaults: {
			placeholder : 'Title'
		},

		_bind: function() {
			var self = this;

			if (! this.$el.length) { return; }

			// ------------------------------------------------------------------------
			
			this.$el.bind('clickoutside', function() {
				if (self.$el.text().replace(/\s+/, '') == '') {
					$(this).addClass('empty-field');
				}
			});

			this.$el.bind('click', function() {
				$(this).removeClass('empty-field');
			});
		},

		init: function(element, options) {
			this.$el     = element;
			this.options = extend(options, this.defaults);

			// ------------------------------------------------------------------------
			
			this.$el.attr('contenteditable', true).
					 attr('data-placeholder', this.options.placeholder).
					 addClass('title-editor-app');

			if (this.$el.text().replace(/\s+/, '') == '' || ((/^<@/).test(this.$el.text()) && (/@>$/).test(this.$el.text()))) { 
				this.$el.addClass('empty-field');
			}

			// ------------------------------------------------------------------------

			this._bind();
		}
	};

	// ------------------------------------------------------------------------

	var App = {
		MainApp 		: void 0,
		$mainBody 		: void 0,
		editorApp 		: void 0,
		ngApp 			: void 0,
		titleEditorApp 	: titleEditorApp,
		embedURL 		: '//emb.keepo.pyo/',
		uploadURL 		: 'api/v1/asset/img',
		editors  		: {title: void 0, lead: void 0, content: void 0},

		_initFileUpload: function($element, options) {
			var self 	= this,
				fileUploadOptions;

			if (! $element.length) { return false; }

			// ------------------------------------------------------------------------
			
			fileUploadOptions = {
				url: self.uploadURL,
				dropZone: void 0,
				/*beforeSend: function(xhr, data) {
			        var file = data.files[0],
			        	temp = JSON.parse(window.localStorage.token);

			        xhr.setRequestHeader('Authorization', temp.token_type + ' ' + temp.access_token);
			    },*/
			    
				add: function(e, data) {
					var that 	= $(this).data('blueimpUIFileupload'),
	                    options = that.options,
	                    files 	= data.files;

	                // Validation
	                // ------------------------------------------------------------------------

                    that._adjustMaxNumberOfFiles(-files.length);
                    data.isAdjusted = true;
                    data.files.valid = data.isValidated = that._validate(files);

                    if (! data.files.valid){
                        that._showError(data.files);
                        return false;
                    }

                    // DOM manipulation
                    // ------------------------------------------------------------------------

                    that.options.dropZone.find('.option-url').hide();
                    that.options.dropZone.siblings('.remove').hide();

                    data.context = that._renderTemplate(data.files).data('data', data);

                    // Trigger
                    // ------------------------------------------------------------------------

                    if ((that._trigger('added', e, data) !== false) &&
                            (options.autoUpload || data.autoUpload) &&
                            data.autoUpload !== false && data.isValidated) {

                        data.submit();
                    }
				},

				done: function(e, data) {
					var that        = $(this).data('blueimpUIFileupload'),
	                    $dropZone   = that.options.dropZone,
	                    context     = $(data.context),
	                    fid, fn, $input;

	                // DOM manipulation
	                // ------------------------------------------------------------------------

	                $dropZone.find('.option-url').show();
                    $dropZone.siblings('.remove').show();

                    // On result
                    // ------------------------------------------------------------------------

	                if (data.result.error) {
	                    data.errorThrown = data.result.error_description || data.result.error;
	                    that._trigger('fail', e, data);
	                } else {
	                	// DOM manipulation
	                	// ------------------------------------------------------------------------

						context.remove();
						
						$dropZone.removeClass('on-progress').addClass('on-preview');
						$dropZone.append(
							$('<div />', {'class': 'preview'}).append(
								$('<img />', {'src': data.result.url})
							).append(
								$('<a />', {'class': 'remove-preview'}).append($('<i />', {'class': 'keepo-icon icon-cancel'}))
							)
						);

						$dropZone.find('.preview').on('click', '.remove-preview', function(e) {
							e.preventDefault();

							$dropZone.find('.preview').remove();
							$dropZone.removeClass('on-preview');

							$dropZone.find('input[type=hidden][name="fid"]').remove();
							$dropZone.find('input[type=hidden][name="fn"]').remove();
						});


						// Append input hidden
						// ------------------------------------------------------------------------

	                    //fid = 'fid_' + $(this).data('id');
	                    if ($dropZone.find('input[type=hidden][name="fid"]').length > 0){
	                        $dropZone.find('input[type=hidden][name="fid"]').val(data.result.id);
	                    }else{
	                        $input = $('<input />', {'type': 'hidden', 'name': 'fid'}).val(data.result.id);
	                        $dropZone.append($input);
	                    }

	                    //fn = 'fn_' + $(this).data('id');
	                    if ($dropZone.find('input[type=hidden][name="fn"]').length > 0){
	                        $dropZone.find('input[type=hidden][name="fn"]').val(data.result.url);
	                    }else{
	                        $input = $('<input />', {'type': 'hidden', 'name': 'fn'}).val(data.result.url);
	                        $dropZone.append($input);
	                    }
	                }
				},

				fail: function(e, data) {
					var that 		= $(this).data('blueimpUIFileupload'),
						$progress 	= data.context.find('.progressbar'),
						$parent 	= data.context,
						$ancestor   = data.context.parent('.fileupload-pool'),
						fid;

					// DOM manipulation
					// ------------------------------------------------------------------------

            		$progress.removeClass('progress-info').addClass('progress-danger');
            		$parent.addClass('has-status')
            			   .append(
            			   		$('<div />', {'class': 'status'}).text(data.errorThrown)
            			   		.append($('<a />', {'class': 'error-close'}).append($('<i />', {'class': 'keepo-icon icon-cancel'})))
            			   	);

            		$parent.on('click', 'a.error-close', function(e) {
            			e.preventDefault();

            			$parent.remove();
            			$ancestor.removeClass('on-progress');
            			$ancestor.find('.helper').show();
            			$ancestor.find('.drop_target').show();
            		});

            		// Remove input hidden
            		// ------------------------------------------------------------------------

            		//fid = 'fid_' + $(this).data('id');
            		data.form.find('input[type=hidden][name=fid]').remove();
            		data.form.find('input[type=hidden][name=fn]').remove();
				}
			};

			fileUploadOptions = $.extend(fileUploadOptions, options);
			$element.fileupload(fileUploadOptions);
		},

		_initEditor: function() {
			var self = this;

			// ------------------------------------------------------------------------
			// Module
			// ------------------------------------------------------------------------

			this.MainApp.keepoApp = angular.module('keepoApp', ['ngSanitize', 'ngTagsInput']);

			// Init controller
			this.MainApp.keepoApp.controller('editorController', ['$scope', '$element', '$http', function($scope, $element, $http) {
				$scope.tempSave = void 0;
				$scope.message  = void 0;
				$scope.tags 	= [];

				$scope.data = {
					title 		: void 0,
					lead 		: void 0,
					content 	: void 0,
					image 		: {
						id 	: void 0,
						url : void 0
					},
					tags 		: [],
					source 		: void 0,
					channel 	: {
						slug : void 0,
						name : void 0
					},
					slug		: void 0
				};

				$scope.data = window.editorData ? $.extend($scope.data, window.editorData) : $scope.data;
				$scope.tags = $scope.data.tags;


				// ------------------------------------------------------------------------
				
				// Tags Autocomplete
				$scope.loadTags = function(query) {
					var config, temp;

					temp = JSON.parse(window.localStorage.token);
					return $http.get(self.MainApp.baseURL + 'api/v1/tags?q=' + query);
				};

				// ------------------------------------------------------------------------

				// Temporary Save
				$scope.saveTemp = function() {
					
				}
				
				// ------------------------------------------------------------------------

				// Save Draft
				$scope.saveDraft = function() {
					var data = $scope._save(angular.copy($scope.data));

					self.prepSave(data, true);
				};

				// ------------------------------------------------------------------------
				
				// Save
				$scope.saveClick = function() {
					var data = $scope._save(angular.copy($scope.data));

					self.prepSave(data);
				};

				// ------------------------------------------------------------------------
				
				$scope._save = function (data) {
					// Cover Image
					var $fid = $($element).find('.fileupload-pool.cover-picture input[type=hidden][name=fid]');
					if ($fid.length) {
						data.image = $.extend(data.image, { id : $fid.val() });
					} else {
						data.image = $.extend(data.image, { id : void 0 });
					}

					// Title and Lead (fuck! I don't know why but contentedit directive sometimes makes noises ಠ_ಠ)
					if ($($element).find('.eb-title').length) {
						data.title = $($element).find('.eb-title').text();
					}
					if ($($element).find('.eb-lead').length) {
						data.lead = $($element).find('.eb-lead').text();
					}

					// Tags
					var tags = [];
					$.each($scope.tags, function() {
						tags.push(this.text);
					});
					data.tags    = tags;

					// Content
					data.content = self.editors.content.serialize()['editor-content'].value.replace(/contenteditable(=(\"|\')true(\"|\')|)/ig, '');

					return data;
				};

				// ------------------------------------------------------------------------
				
				// Browse Cover Picture File
				$scope.browseFile = function (event) {
					var $el = $(event.currentTarget || event.srcElement);

					$el.parent('.fileupload-pool').find('.file-upload').trigger('click');
				};

				// ------------------------------------------------------------------------
				
				// Remove Preview
				$scope.removePreview = function(event) {
					var $el = $(event.currentTarget || event.srcElement);

					$scope.data.image.id = void 0;
				};

				// ------------------------------------------------------------------------
				
				// Get Image from URL
				$scope.getImage = function(event) {
					var $el = $(event.currentTarget || event.srcElement);

					console.log('asd');
				};

				// ------------------------------------------------------------------------
				
				// Set Channel
				$scope.setChannel = function($event, slug) {
					var $el = $(event.currentTarget || event.srcElement);

					$scope.data.channel = {
						slug : slug,
						name : $el.text()
					}
				};

				// ------------------------------------------------------------------------
				
				// Close Message popup
				$scope.closeMessage = function($event) {
					$scope.message = void 0;
				};
			}]);

			this.MainApp.keepoApp.directive('droppable', function() {
				return {
					scope: {},
					link: function(scope, element) {
						var el = element[0];

						el.addEventListener(
						    'dragover',
						    function(e) {
						        e.dataTransfer.dropEffect = 'move';
						        // allows us to drop
						        if (e.preventDefault) e.preventDefault();
						        this.classList.add('over');
						        return false;
						    },
						    false
						);

						el.addEventListener(
						    'dragenter',
						    function(e) {
						        this.classList.add('over');
						        return false;
						    },
						    false
						);

						el.addEventListener(
						    'dragleave',
						    function(e) {
						        this.classList.remove('over');
						        return false;
						    },
						    false
						);
					}
				}
			});

			/*this.MainApp.keepoApp.directive('contentedit', function() {
				return {
				    require: 'ngModel',
				    link: function(scope, element, attrs, ngModel) {

				      function read() {
				        ngModel.$setViewValue(element.html());
				      }

				      ngModel.$render = function() {
				        element.html(ngModel.$viewValue || "");
				      };

				      element.bind('blur keyup change', function() {
				        scope.$apply(read);
				      });
				    }
				  };
			});*/

			// ------------------------------------------------------------------------
			// Upload Cover Picture
			// ------------------------------------------------------------------------
			
			if (this.$mainBody.find('.fileupload-pool.cover-picture').length) {
				this._initFileUpload(this.$mainBody.find('.fileupload-pool.cover-picture input[type=file]'), {dropZone: this.$mainBody.find('.fileupload-pool.cover-picture')});
			}

			// ------------------------------------------------------------------------
			// Medium Editor Setup
			// ------------------------------------------------------------------------

			/*// Content
			if (this.$mainBody.find('.eb-article').length) {
				var contentEditor = new Dante.Editor({
					el: '.eb-article',
					disable_title: true,
					base_widgets: ["uploader", "embed"],
					base_behaviors: ["image", "paste", "list"],
					body_placeholder: 'Write your content here ------- Write then block to show text tools',
					default_loading_placeholder: this.MainApp.baseURL + 'css/images/editor-icons/media-loading-placeholder.png',
					related_url: this.embedURL + 'related?url=',
					oembed_url: this.embedURL + 'oembed?url=',
					extract_url: this.embedURL + 'extract?url='
				});
				contentEditor.start();

				this.editors.push(contentEditor);
			}*/

			// Wait until angular finished rendering ¯\_(ツ)_/¯
			setTimeout(function() {
				// Title
				if (self.$mainBody.find('.eb-title').length) {
					var titleEditor = new titleEditorApp(self.$mainBody.find('.eb-title'), {
						placeholder: 'Title'
					});

					self.editors.title = titleEditor;
				}

				// Lead
				if (self.$mainBody.find('.eb-lead').length) {
					var titleEditor = new titleEditorApp(self.$mainBody.find('.eb-lead'), {
						placeholder: 'Lead (subtitle) this text will appear in thumbnail'
					});

					self.editors.lead = titleEditor;
				}

				// Content
				if (self.$mainBody.find('.eb-article').length) {
					var contentEditor = new MediumEditor('#editor-content', {
						toolbar: {
							buttons: ['bold', 'italic', 'underline', 'anchor', 'h1', 'h2', 'quote', "orderedlist", "unorderedlist"],
						},
						paste: {
							cleanPastedHTML: true,
							cleanTags: ["meta", "script", "style", "label"]
						},
						placeholder: {
							text: 'Write your content here ------- Write then block to show text tools'
						}
					});

					$('#editor-content').mediumInsert({
				        editor: contentEditor,
				        addons: {
				        	images: {
				        		deleteScript: null,
				        		autoGrid: 0,
				        		fileUploadOptions: {
				        			url: self.uploadURL,
			        				beforeSend: function(xhr, data) {
			        			        var file = data.files[0],
			        			        	temp = JSON.parse(window.localStorage.token);

			        			        xhr.setRequestHeader('Authorization', 'Bearer ' + temp.token);
			        			    }
				        		},
				        		styles: {
				        		    wide: { label: '<span class="icon-align-justify"></span>' },
				        		    left: null,//{ label: '<span class="icon-align-left"></span>' },
				        		    right: { label: '<span class="icon-align-right"></span>' },
				        		    grid: null
				        		}
				        	},
				        	embeds: {
				        		placeholder: 'Paste a YouTube, Facebook, Twitter, Instagram link/video and press Enter',
            					oembedProxy: '',
				        		styles: {
				        		    wide: null,
				        		    left: null,
				        		    right: null
				        		}
				        	}
				        }
				    });

					self.editors.content = contentEditor;
				}
			}, 50);

			

			// ------------------------------------------------------------------------

			this.MainApp.keepoApp.config([
			    '$compileProvider', '$interpolateProvider', '$sceProvider', '$httpProvider',
			    function ($compileProvider, $interpolateProvider, $sceProvider, $httpProvider) {
			        // whitelist href content
			        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|javascript):/);
			        // disable debug info
			        $compileProvider.debugInfoEnabled(true);
			        // change binding notation
			        $interpolateProvider.startSymbol('<@');
			        $interpolateProvider.endSymbol('@>');
			        // disable SCE
			        $sceProvider.enabled(false);

			        // Set Authorization (Assuming token already stored in localStorage)
			        var temp = JSON.parse(window.localStorage.token);
			        $httpProvider.defaults.headers.common = {
			        	'Authorization': 'Bearer ' + temp.token
			        };
			    }
			]);

			// start up the module
			if (! this.editorApp || (this.editorApp == 'article')) {
				this.MainApp.keepoApp.run();
				angular.bootstrap(document.querySelector("html"), ["keepoApp"]);
			}
		},

		// ------------------------------------------------------------------------

		init: function(MainApp) {
			var self = this;

			// Initialize variables
			this.MainApp        = MainApp;
			this.$mainContainer = $('.main-container');
			this.$mainBody      = $('.editor-container');
			this.editorApp      = this.$mainBody.data('editor');
			this.uploadURL 		= MainApp.baseURL + this.uploadURL;

			// Initialize editor
			if (! self.editorApp || (self.editorApp == 'article'))
			{ self._initEditor(); }
			else {
				require(['apps/editors/' + self.editorApp + '.module'], function(EditorApp) {
					EditorApp.init(self);
				});
			}
		},

		prepSave: function(data, draft) {
			var self 		= this,
				draft 		= draft || false,
				prepData 	= {};

			if (this.$mainContainer.hasClass('on-progress')) { return false; }

			// ------------------------------------------------------------------------
			
			// Set up the data
			prepData = {
				cover 		: data.image.id || 0,
				title 		: data.title ? data.title : 'Untitled',
				lead 		: data.lead,
				content 	: data.content,
				tags 		: data.tags.join(';'),
				source 		: data.source ? ((/^http\:\/\//).test(data.source) ? data.source : 'http://' + data.source) : '',
				channel 	: data.channel.slug,
				type 		: this.editorApp,
				status  	: draft ? 'draft' : 'submit',
				slug 		: data.slug
			};

			// ------------------------------------------------------------------------
			
			this.$mainContainer.addClass('on-progress');
			this.save(prepData);
		},

		save: function(data) {
			var self 	= this,
				method  = !data.slug ? 'POST' : 'PUT',
				$scope 	= angular.element('[ng-controller=editorController]').scope();

			// ------------------------------------------------------------------------
			
			// Now let's try to post it
			$.ajax({
				url  	: this.MainApp.baseURL + 'api/v1/feed',
				data 	: data,
				method 	: method
			}).
			done(function(response) {
				// Remove loading status
				self.$mainContainer.removeClass('on-progress');

				// Got slug in the response? then it must be edit
				if (response.slug) {
					MainApp.defaultPopUp({
						title: 'Saved!',
						info:  response.status ? 'Your issue has been saved'  : 'Your issue has been updated'
					});

					$scope.data.slug = response.slug;
					$scope.$apply();

					return;
				}

				// Set message popup
				if (data.status == 'submit') {
					$scope.message = {success: true};
					$scope.$apply();

					return;
				}
			}).
			fail(function(response) {
				// Remove loading status
				self.$mainContainer.removeClass('on-progress');

				// Get the reason
				if (response.responseJSON) { var reason = response.responseJSON.error_description; }
				else { var reason = JSON.parse(response.responseText).error_description; }

				// Set message popup
				$scope.message = {error: reason};
				$scope.$apply();
			});
		}
	};

	return App;
});