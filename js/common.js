define([
    // Defaults
    "jquery"
], function ($) {
    // ------------------------------------------------------------------------
    // Search
    // ------------------------------------------------------------------------

    // Nav
    $(".main-nav .search a").click(function(e){
        e.preventDefault();

        if ($(this).parent().hasClass('show')){
            $(this).parent().removeClass('show');
            $(".search-bar").stop().slideUp();
        } else{
            $(this).parent().addClass('show');
            $(".search-bar").stop().slideDown();
        }
    });
    /*
    $(".main-nav .more").mouseenter(function(){
        if (!$(".main-nav .search").hasClass('show')){
            console.log('aaa');
            $(this).parent().find('.submenu').show();
        } else{
            console.log('bbb');
        }
    });

    $(".main-nav .more").mouseleave(function(){
        $(this).parent().find('.submenu').hide();
    });
    */

    // ------------------------------------------------------------------------
    // Post Editor Popup
    // ------------------------------------------------------------------------

    $('.quick-nav').on('click', '.post', function(e) {
        e.preventDefault();

        // Do we have #not-activated-yet element?
        if ($('#not-activated-yet').length) {
            $('#not-activated-yet').removeClass('hidden');
            return false;
        }

        // ------------------------------------------------------------------------
        

        // Open post editor popup
        $('#epi').removeClass('hidden').addClass('epi-opened');

        setTimeout(function() {
            $('#epi .epi-close').removeClass('hidden');
            $('#epi .epi-main-header').removeClass('hidden');
        }, 1500);
    });

    $('#epi').on('click', '.epi-close', function(e) {
        e.preventDefault();

        // Add class for closing animation
        $('#epi').addClass('epi-closed');

        setTimeout(function() {
            $('#epi').addClass('hidden').removeClass('epi-opened epi-closed');

            $('#epi .epi-close').addClass('hidden');
            $('#epi .epi-main-header').addClass('hidden');
        }, 1500);    
    });

    if ($('#not-activated-yet').length) {
        $('#not-activated-yet').on('click', '.nay-close', function(e) {
            e.preventDefault();

            $('#not-activated-yet').addClass('hidden');
        });
    }

});